\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Project description}{1}
\contentsline {subsection}{\numberline {2.1}Data-mining}{1}
\contentsline {subsection}{\numberline {2.2}Data-prcessing and Data-displaying}{1}
\contentsline {subsection}{\numberline {2.3}Data publication}{1}
\contentsline {section}{\numberline {3}Development Tools}{1}
\contentsline {subsection}{\numberline {3.1}Data}{1}
\contentsline {subsubsection}{\numberline {3.1.1}APIs}{1}
\contentsline {subsection}{\numberline {3.2}API calls}{1}
\contentsline {subsection}{\numberline {3.3}Programming Languages}{2}
\contentsline {subsubsection}{\numberline {3.3.1}Python}{2}
\contentsline {subsubsection}{\numberline {3.3.2}Angular}{2}
\contentsline {subsubsection}{\numberline {3.3.3}JS}{2}
\contentsline {subsection}{\numberline {3.4}Utils}{2}
\contentsline {subsubsection}{\numberline {3.4.1}GitLab}{2}
\contentsline {subsubsection}{\numberline {3.4.2}Linter}{2}
\contentsline {section}{\numberline {4}First Steps}{2}
