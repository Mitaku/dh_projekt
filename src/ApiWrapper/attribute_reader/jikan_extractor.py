import json

class JikanEX():

    @staticmethod
    def load_list():
        json_data = open("attribute_reader/attributes.json").read()
        data = json.loads(json_data)
        return data

    @staticmethod
    def identify(text):
        for attributes in JikanEX.load_list()['attributes']:
            for identifier in attributes['identifier']:
                matches = [x for x in text if x.contains(identifier)]
                print(matches)