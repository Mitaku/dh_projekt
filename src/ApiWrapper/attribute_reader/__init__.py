"""Module for Extracting Information out of an Text using NLTK <br>
January 18, 2019
author: Michael Fritz
"""

from .informationextractor import IEModule
from .jikan_extractor import JikanEX
from .topic_modelling import TopicModelling