"""
Module for Information Extraction <br>
Created on Jan 18, 2019

author: Michael Fritz
"""
import re
import nltk
from nltk import tree2conllstr
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

#nltk.download()

class IEModule():
    """
    Class for Information Extraction
    """

    def __init__(self, document):
        """
        :param document: Text to extract information from
        """
        self.document = document

    def ie_preprocess(self):
        """Preprocess on input data: <br>
        1. sentence segmention <br>
        2. word tokenisation <br>
        3. POS-tagging """
        sentences = nltk.sent_tokenize(self.document)      ##sentence segmenter
        sentences_splitted = []
        for sent in sentences:
            sentences_splitted.extend(re.split("\r", sent))
        sentences = sentences_splitted
        words = \
            [self.word_preprocess(nltk.word_tokenize(sent)) for sent in sentences] ##word tokenizer
        pos_tagged_sentences = \
            [nltk.pos_tag(word) for word in words]     ##part-of-speech tagger
        return pos_tagged_sentences

    @staticmethod
    def word_preprocess(words):
        """Proprocessing for Words [EMPTY]"""
       ## words = self.delete_stopwords(words)
       ## words = self.stemming_words(words)
       ## words = self.lemmatize(words)
        return words

    @staticmethod
    def delete_stopwords(text):
        """Delete english stopwords from list of words"""
        stop_words = set(stopwords.words("english"))
        return [w for w in text if not w in stop_words]

    @staticmethod
    def stemming_words(words):
        """setmming a list of words"""
        stemmer = PorterStemmer()
        #sn = SnowballStemmer("english")
        return [stemmer.stem(w) for w in words]

    @staticmethod
    def lemmatize(words):
        """lemmatize a list of words"""
        lemmatizer = WordNetLemmatizer()
        return [lemmatizer.lemmatize(w) for w in words]

    def np_chunking(self, pt_sentences):
        """NP-chunking with an basic Grammer: <br>
        NP: {<DT>?<JJ>*<N[N|E]>+}"""
        grammer = r"""NP: {<DT>?<JJ>*<N[N|E]>+}"""
        parser = nltk.RegexpParser(grammer)
        pt_sentences = self.ner(pt_sentences)       ##ner chunking
        result = parser.parse(pt_sentences)
        return result

    @staticmethod
    def ner(tagged):
        """Named Entity Tagging"""
        named_ent = nltk.ne_chunk(tagged, binary=True)
        return named_ent

    def exec(self):
        """Execute preprocess+ np-chunking"""
        word_list = []
        for pt_sentence in self.ie_preprocess():
            word_list.append(self.np_chunking(pt_sentence))

        return word_list
           ##self.np_chunking(pt_sentence).draw()
            #iob_tagged = tree2conllstr(self.np_chunking(pt_sentence))
            #ne_tree = nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(self.document)))
            #print(ne_tree)
