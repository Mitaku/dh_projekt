import spacy
from spacy.lang.en import English
import nltk
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
from gensim import corpora
import pickle
import gensim


spacy.load('en')
parser = English()
en_stop = set(nltk.corpus.stopwords.words('english'))

class TopicModelling():

    @staticmethod
    def tokenize(text):
        lda_tokens = []
        tokens =parser(text)
        for token in tokens:
            if token.orth_.isspace():
                continue
            elif token.like_url:
                lda_tokens.append('URL')
            elif token.orth_.startswith('@'):
                lda_tokens.append('SCREEN_NAME')
            else:
                lda_tokens.append(token.lower_)
        return lda_tokens

    @staticmethod
    def get_lemma(word):
        lemma = wn.morphy(word)
        if lemma is None:
            return word
        else:
            return lemma
    @staticmethod
    def get_lemma2(word):
        return WordNetLemmatizer().lemmatize(word)

    @staticmethod
    def prepare_text_for_lda(text):
        tokens = TopicModelling.tokenize(text)
        tokens = [token for token in tokens if len(token) > 4]
        tokens = [token for token in tokens if token not in en_stop]
        tokens = [TopicModelling.get_lemma(token) for token in tokens]
        return tokens

    @staticmethod
    def lda(text_data):
        NUM_TOPICS = 5
        dictionary = corpora.Dictionary(text_data)
        corpus = [dictionary.doc2bow(text) for text in text_data]

        pickle.dump(corpus, open('corpus.pkl', 'wb'))
        dictionary.save('dictionary.gensim')

        ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=NUM_TOPICS, id2word=dictionary, passes=15)
        ldamodel.save('model5.gensim')

        topics = ldamodel.print_topics(num_words=4)
        for topic in topics:
            print(topic)