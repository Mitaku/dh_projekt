"""
Module for character information extraction from wikia site <br>
created: January 27, 2019
version: 0.1
updated: January 27, 2019
author: Michael Fritz
"""

from requests_html import HTMLSession

class Char:
    """Character object"""

    def __init__(self, _site):
        self._site = _site

    def get_info(self):
        """
        Get all character urls
        :return: List of urls
        """
        print("open: " + self._site)
        session = HTMLSession()
        page = session.get(self._site)


        text_html = page.html.find('#mw-content-text')
        infobox = page.html.find('.infobox')
        #infobox can be other! .portable-infobox

        text = text_html[0]
        text_list = self.ie(text)

        if len(infobox) > 0:
            info_list = []
            for tr in infobox[0].find('tr'):
                td_list = tr.find('td')
                if len(td_list) == 2:
                    info_list.append((td_list[0].text, td_list[1].text))
            return info_list, text_list
        else:
            info_list = []
            if len(page.html.find('.portable-infobox')) > 0:
                label = page.html.find('.pi-data-label')
                value = page.html.find('.pi-data-value')
                if not (label == -1 or value == -1):
                    for n in range(len(label)):
                        info_list.append((label[n].text,value[n].text))
                    return info_list,text_list

        return [],[]


    def ie(self, text):
        """
        Methode to split text from the html-website into clean text
        :return: list of header with body as tupel
        """
        text_list = []
        selectors = []
        for h2 in text.find('h2'):
            selectors.append(h2.text)
        for n in range(len(selectors)-1):
            header = selectors[n]
            des = text.text[text.text.find(selectors[n]):text.text.find(selectors[n+1])]
            des = des.replace(header, "")
            header = header.replace("Edit", "").replace(" ", "")
            #Filter
            if(header == "Appearance" or header == "Personality"):
                text_list.append((header, des))

        return text_list