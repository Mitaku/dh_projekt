"""
Module for Wikia-site search <br>
created: January 27, 2019
version: 0.1
updated: January 27, 2019
author: Michael Fritz
"""

from ._constants import SESSION
from ._constants import PATH_BASE

CHAR_PATH = PATH_BASE+'Wikis/ByString'


class Wiki:
    """Wiki object"""

    def __init__(self, query):
        self.query = query

    def search(self):
        """
        Search all Wikis by String
        :return: JSON onject of wikis
        """
        path = CHAR_PATH+"?string="+self.query;
        response = SESSION.get(path)
        return response.json()

    def getEn(self):
        """
        Filter all wikis for englisch ones
        :return: englisch wikis
        """
        response = self.search()
        list = response["items"]
        en_list = [];
        for element in list:
            if element['language'] == 'en':
                en_list.append(element)
        return en_list

    def getDomain(self):
        """
        Return domain of first englisch wiki
        :return: englisch wiki with lowest id
        """
        en = self.getEn();
        if len(en) > 0:
            return self.getEn()[0]['domain']
        else:
            return "404"