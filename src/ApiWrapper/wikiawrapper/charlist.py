"""
Module for character extraction from wikia site <br>
created: January 27, 2019
version: 0.1
updated: January 27, 2019
author: Michael Fritz
"""

import requests
from requests_html import HTMLSession

class CharList:
    """Character object"""

    def __init__(self, _site):
        self._site = _site

    def getList(self):
        """
        Get all character urls
        :return: List of urls
        """
        print("open: " + self._site)
        session = HTMLSession()
        page = session.get(self._site)
        chars_html = page.html.find('.category-page__members')
        if len(chars_html) > 0:
            char_links = chars_html[0].absolute_links
            char_list = []
            for charl in char_links:
                if "Category:" not in charl:
                    char_list.append(charl)
            #delete all with Category:... TODO
            return char_list,1
        else:
            return [],0

