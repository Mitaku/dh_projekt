"""
Constants for Wikia Wrapper
"""

import requests

SESSION = requests.Session()
"""Constant of Session for all API calls"""
SESSION.params = {}

PATH_BASE = 'http://www.wikia.com/api/v1/'
"""Base Path for all API calls"""
