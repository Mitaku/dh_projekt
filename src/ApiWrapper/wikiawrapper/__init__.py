"""
Module for managing API calls to wikia.com/api <br>
January 27, 2019 <br>
author: Michael Fritz
"""

from .wiki import Wiki
from .charlist import CharList
from .char import Char
