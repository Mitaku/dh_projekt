"""
Constants for API Wrapper
"""

import requests

SESSION = requests.Session()
"""Constant of Session for all API calls"""
SESSION.params = {}

PATH_BASE = 'https://api.jikan.moe/v3/'
"""Base Path for all API calls"""
