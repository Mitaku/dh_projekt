"""
Module for managing API calls to api.jikan.moe <br>
January 18, 2019 <br>
author: Michael Fritz
"""

import requests
from .char import Char
from .anime import Anime
from .top import Top