"""
Top-Anime-List of API wrapper <br>
"""

from ._constants import SESSION
from ._constants import PATH_BASE
from re import sub

TOP_PATH = PATH_BASE+"top/anime/{}"


class Top:
    """Class to get Top List of Animes"""

    def __init__(self, _page):
        """
        :param _page: page for toplist search
        """
        self._page = _page

    def getAll(self):
        """Get a page (50) of TopANnme
        :return: returns list as json
        """
        path = TOP_PATH.format(self._page)
        response = SESSION.get(path)
        return response.json()

    def getNameESC(self):
        """
        Get a list of all Animes escaped from ':'
        :return:
        """
        list = self.getAll()
        esc_list = []
        for element in list['top']:
            esc_list.append(self.remove(element['title']).replace(" ","+"))
        return esc_list

    def remove(self, text):
        text = sub('\([0-9]*\)','',text) #delete years after show  fex. (2011)
        return sub('[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890;\- ]', '', text)