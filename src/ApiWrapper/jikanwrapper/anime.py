"""Anime Object of API wrapper <br>
All calls for anime
"""

from ._constants import SESSION
from ._constants import PATH_BASE

ANIME_PATH = PATH_BASE+'anime/{}'


class Anime():
    """Anime Class for all calls to an specific anime by id"""

    def __init__(self, _id):
        """
        :param id: ID of anime
        """
        self._id = _id

    def all(self):
        """Get all information to the anime
        :return: returns anime as json
        """
        path = ANIME_PATH.format(self._id)
        response = SESSION.get(path)
        return response.json()

    def staff(self):
        """
        Get all characters and staff of an anime
        :return: list of characters and list of staff
        """
        path = ANIME_PATH.format(self._id)+"/characters_staff"
        response = SESSION.get(path)
        return response.json()

    def chars(self):
        """
        filter staff for only characters
        :return: list of characters
        """
        response = self.staff()
        return response["characters"]

    def genres(self):
        """
        :return: list of genres
        """
        response = self.all()
        return response["genres"]
