"""
Module for Characters <br>
created: January 18, 2019
version: 0.1
updated: January 18, 2019
author: Michael Fritz
"""

from ._constants import SESSION
from ._constants import PATH_BASE

CHAR_PATH = PATH_BASE+'character/{}'


class Char():
    """Character object"""

    def __init__(self, _id):
        """
        :param id: Id of character
        """
        self._id = _id

    def all(self):
        """
        Get all information of Character by ID
        :return: JSON onject of character
        """
        path = CHAR_PATH.format(self._id)
        response = SESSION.get(path)
        return response.json()

    def about(self):
        """
        Filter all information for about
        :return: description of character
        """
        response = self.all()
        return response["about"]
