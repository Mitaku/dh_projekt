"""Methodes for testing API calls to jikan API"""

import pytest
from pytest import fixture
import vcr
from jikanwrapper import Char
from jikanwrapper import Anime


@fixture
def char_keys():
    """important keys for Chracter return JSON"""
    return ['mal_id', 'name', 'about']

@fixture
def anime_keys():
    """important keys for Anime return JSON"""
    return ['mal_id', 'title', 'genres']

@fixture
def staff_keys():
    """important keys for staff return JSON"""
    return ['characters']


#@pytest.mark.skip(reason="Already Tested")
@vcr.use_cassette('tests/vcr_cassettes/char-about.yml')
def test_char_all(char_keys):
    """Test an API call to get a character"""
    char_instance = Char(12)
    response = char_instance.all()
    assert isinstance(response, dict)
    assert response['mal_id'] == 12, "Id needed to be responded"
    assert set(char_keys).issubset(response.keys()), "All relevant keys needed to be responsed"


@vcr.use_cassette('tests/vcr_cassettes/anime.yml')
def test_anime_all(anime_keys):
    """Test an API call to get an Anime"""
    anime_instance = Anime(1220)
    response = anime_instance.all()
    assert isinstance(response, dict)
    assert response['mal_id'] == 1120, "Id needed to be responded"
    assert set(anime_keys).issubset(response.keys()), "All relevant keys needed to be responsed"

def test_anime_char(staff_keys):
    """Test call to get all characters of an anime"""
    anime_instance = Anime(1220)
    response = anime_instance.staff()
    assert isinstance(response, dict)
    assert set(staff_keys).issubset(response.keys()), "All relevant keys needed to be responsed"
