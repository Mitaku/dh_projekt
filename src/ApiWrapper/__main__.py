from wikiawrapper import Wiki
from jikanwrapper import Top
from wikiawrapper import CharList
from wikiawrapper import Char
from models import Anime_model
from models import Char_model
from attribute_reader import TopicModelling
import random

toplist1 = Top(1).getNameESC()
toplist1 = list(set(toplist1)) #delete duplicates


anime_list = []

#test only!
#toplist1 = toplist1[15:]

domainlist1 = []
for element in toplist1:
    domain = Wiki(element).getDomain()
    if domain != "404":
        domainlist1.append("https://"+domain+"/wiki/Category:Characters")
        anime = Anime_model(element,domain) #TODO title
        list,err = CharList("https://"+domain+"/wiki/Category:Characters").getList()
        if err != 0:
            for char in list:
                infobox, text = Char(char).get_info()
                name = char.replace("https://"+domain+"/wiki/","").replace("_"," ")
                #topic modelling text!
                #if len(text) > 0:
                    #tokens = TopicModelling.prepare_text_for_lda(text[0][1]) #personality
                    #TopicModelling.lda(tokens)
                    #print(tokens)
                anime.addChar(Char_model(name,infobox,text))
        print(anime.toString())
        anime.save_DB()