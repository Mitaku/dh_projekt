
class Char_model:

    def __init__(self, name, infobox, text):
        self.name = name
        self.infobox = infobox
        self.text = text

    def toString(self):
        inboxstr = ""
        txtstr = ""
        for inb in self.infobox:
            inboxstr += ",".join(inb) +";"
        inboxstr[:len(inboxstr)-1]
        for txt in self.text:
            txtstr += ",".join(txt) +";"
        txtstr[:len(txtstr)-1]

        return self.name+": "+inboxstr+" - "+txtstr