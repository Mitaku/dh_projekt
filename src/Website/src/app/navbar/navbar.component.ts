import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
          width: '20%'
      })),
      state('closed', style({
        width: '5%'
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.2s')
      ])
    ])
  ]
})
export class NavbarComponent implements OnInit {

  public isOpen = true;
  toggle() {
    this.isOpen = !this.isOpen;
  }

  constructor(_router: Router) { }

  ngOnInit() {
  }

}
