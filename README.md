# DH_Projekt

## Development with Python
* Documentation
    * pdoc3 0.5.1
    * Create Dokumentation: `pdoc --html --html-dir doc <ModuleName>`
    * use `--override` to override old documentation
    * Documentation ends under src/doc
* Testing
    * pytest and vcrpy
    * verify with `assert`
    * use command `py.test <ModuleName>` to test
    * test Methods / test Files had to start with: `test_`
* Linter
    * CI for pylint
    * use `pylint <ModuleName>` to test locally


